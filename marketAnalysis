with mktg_spend as (
            select 
            date_trunc('week',reporting_date)::date::date as spend_week
            ,date_part('week',reporting_date) as spend_week_num
            ,date_part('year',reporting_date) as spend_year
            ,brand_not_brand
            ,region
            , case
            when campaign LIKE '%_apac_%' then 'APAC'
            when campaign LIKE '%_APAC_%' then 'APAC'
            when campaign LIKE '%_emea_%' then 'EMEA'
            when campaign LIKE'%_EMEA_%' then 'EMEA'
            when campaign LIKE '%_amer_%' then 'AMER'
            when campaign LIKE '%_AMER_%'then 'AMER'
            when campaign LIKE '%_sanfrancisco_%' then 'AMER'
            when campaign like '%_seattle_%'then 'AMER'
            when campaign LIKE '%_lam_%' then 'LATAM'
            when campaign LIKE '%_sam_%' then 'LATAM'
            else 'Global' end as region_normalized
            ,case
            when medium = 'cpc' then 'Paid Search'
            when medium = 'display' then 'Display'
            when medium = 'paidsocial' then 'Paid Social'
            else 'Other' end as mapped_channel
            , case
            when source = 'google' then 'Google'
            when source = 'bing_yahoo' then 'Bing'
            when source = 'facebook' then 'Facebook'
            when source = 'linkedin' then 'LinkedIn'
            when source = 'twitter' then 'Twitter'
            else 'Other' end as mapped_source
            ,CASE 
             WHEN budget LIKE '%x-ent%' THEN 'Large'
             WHEN budget LIKE '%x-mm%' THEN 'Mid-Market'
             WHEN budget LIKE '%x-smb%' THEN 'SMB'
             WHEN budget LIKE '%x-pr%' THEN 'Prospecting'
             WHEN budget LIKE '%x-rtg%' THEN 'Retargeting'
             ELSE NULL
             END AS utm_segment
             ,sum(cost) as total_cost
            from legacy.pmg_paid_digital
            group by 1,2,3,4,5,6,7,8,9
            having total_cost > 0
  ),
  
  oppty_base as (
      select b.*, 
                b1.OPPORTUNITY_SALES_DEVELOPMENT_REPRESENTATIVE, 
                case when e.order_type_name = '1. New - First Order' then 'FO' else 'Not FO' end as FO_Not_FO_Flag,
                f.crm_account_Name, 
                 f.parent_crm_account_name,
                 d.SALES_SEGMENT_NAME
      from common.fct_crm_opportunity as b
     -- lead created date should be within 1 yr of opp created date
       inner join "PROD"."COMMON"."DIM_CRM_OPPORTUNITY" b1
          on b1.DIM_CRM_OPPORTUNITY_ID = b.dim_crm_opportunity_id
          -- and sales_type = 'New Business'
          and stage_name NOT IN ('00-Pre Opportunity', '10-Duplicate')
       left join "PROD"."COMMON"."DIM_ORDER_TYPE" as e
            on b.dim_order_type_id = e.dim_order_type_id
       left join "PROD"."COMMON"."DIM_SALES_SEGMENT" as d
            on b.DIM_PARENT_SALES_SEGMENT_ID = d.DIM_SALES_SEGMENT_ID
       left join "PROD"."COMMON"."DIM_CRM_ACCOUNT" f
    on f.dim_crm_account_id = b.dim_crm_account_id
      where b.net_arr > 0
),

    saos as (
  select a.*, 
          dim_date.fiscal_year,
          case when b.mrr_sum_6mo > 0 then 'Growth'
                 when c.mrr_sum_6mo > 0 then 'Connected New'
                 else 'FO' end as oppty_type
  from oppty_base a
  left join (select b1.DIM_CRM_ACCOUNT_ID, sum(mrr) as mrr_sum_6mo
            from "PROD"."COMMON_MART_SALES"."MART_ARR" as b1
            inner join oppty_base as b2
                on b1.DIM_CRM_ACCOUNT_ID = b2.DIM_CRM_ACCOUNT_ID
            where b1.arr_month between b2.created_date - 180 and b2.created_date
            group by 1) as b
    on a.DIM_CRM_ACCOUNT_ID = b.DIM_CRM_ACCOUNT_ID
  left join (select c1.DIM_PARENT_CRM_ACCOUNT_ID, sum(mrr) as mrr_sum_6mo
            from "PROD"."COMMON_MART_SALES"."MART_ARR" as c1
            inner join oppty_base as c2
                on c1.DIM_PARENT_CRM_ACCOUNT_ID = c2.DIM_PARENT_CRM_ACCOUNT_ID
            where c1.arr_month between c2.created_date - 180 and c2.created_date
            group by 1) as c
    on a.DIM_PARENT_CRM_ACCOUNT_ID = c.DIM_PARENT_CRM_ACCOUNT_ID
  LEFT JOIN common.dim_date 
  ON a.created_date = dim_date.date_day
),

mktg_spend_sum as (
  
  select spend_week, spend_week_num, spend_year, --utm_segment, 
    sum(total_cost) as mktg_spend
  from mktg_spend
  group by 1,2,3--,4
),

sao_sum as (
        select date_trunc('week', created_date) as oppty_week, 
              count(*) as num_fo_opptys,
              sum(case when sales_segment_name = 'SMB' then 1 else 0 end) as num_smb_fo_opptys,
              sum(case when is_won then 1 else 0 end) as num_won_fo_opptys,
              sum(case when is_won then net_arr else 0 end) as num_won_fo_net_arr
        from saos
        where created_date >= '2019-01-01'
              and oppty_type = 'FO'
        group by 1
        order by 1
  )
  
  select *
  from sao_sum as a
  //inner join mktg_spend_sum as b
  //  on b.spend_week = a.oppty_week   

